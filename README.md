# fedora-desktop-packages

This is a spot to house spec files and build scripts for some desktop things I want and that I intend to put in a COPR until Fedora catches up to upstream.

These are provided as-is, in the hope that they will be useful to someone else also. There is no guarantee that I can provide support, but if you run into any problems please open an issue and/or a merge request here.

All of these tools are the works of their respecvtive authors, and are used according to the terms of their Free Software licenses. I have merely aggregated them and packaged them in a convenient way for users of Fedora Workstation 30.

To make these packages available on your Fedora 30 systems, use dnf to enable the repositories:

```text
sudo dnf copr enable geoffbeier/weechat
sudo dnf copr enable geoffbeier/sway
```

## weechat

The weechat RPM that shipped with F30 is 2.4. The remote protocol has improved significantly with 2.5.

[![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/weechat/package/weechat/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/weechat/package/weechat/)

For more information on weechat, see [its github page](https://github.com/weechat) or [its homepage](https://weechat.org).

## sway

F30 still ships a fairly old `sway` package. The sway-related packages here are the ones I use to set up a current sway-based environment.

The spec files are based on ones [published by Marvin Beckers](https://github.com/embik/fedora-packages) as well as ones proposed for current Rawhide, with modifications as needed for current versions of the projects and tweaks to make it easier for me to run test builds then push to a COPR.

| Package       | Build Status |
| ------------- | ------------ |
| [wlroots](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/wlroots/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/wlroots/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/wlroots/) |
| [xcb-util-errors](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/xcb-util-errors/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/xcb-util-errors/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/xcb-util-errors/) |
| [sway](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/sway/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/sway/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/sway/) |
| [swaybg](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swaybg/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swaybg/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swaybg/) |
| [swaylock](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swaylock/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swaylock/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swaylock/) |
| [swayidle](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swayidle/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swayidle/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/swayidle/) |
| [waybar](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/waybar/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/waybar/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/waybar/) |
| [mako](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/mako/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/mako/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/mako/) |
| [grim](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/grim/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/grim/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/grim/) |
| [slurp](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/slurp/) | [![Copr build status](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/slurp/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/geoffbeier/sway/package/slurp/) |

Most of these packages can be found in the [swaywm repositories on github](https://github.com/swaywm/). [xcb-util-errors](https://cgit.freedesktop.org/xcb/util-errors/) can be found at freedesktop.org. Waybar [lives here](https://github.com/Alexays/Waybar). Mako is from [Simon Ser's github repository](https://github.com/emersion/mako). So is [grim](https://github.com/emersion/grim). And also [slurp](https://github.com/emersion/slurp).

Waybar requires the `fmt` library, which is built from [the 5.3.0 rawhide spec file and included in the COPR](https://src.fedoraproject.org/rpms/fmt.git).
