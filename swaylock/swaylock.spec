Name:       swaylock
Version:    1.3
Release:    2%{dist}
Summary:    Screen locker for Wayland

License:    MIT
URL:        https://github.com/swaywm/swaylock
Source0:    %{url}/archive/%{version}/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:    %{url}/releases/download/%{version}/%{name}-%{version}.tar.gz.sig

BuildRequires:  gcc
BuildRequires:  meson >= 0.48.0
BuildRequires:  pam-devel
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-protocols) >= 1.14
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  scdoc

%description
swaylock is a screen locking utility for Wayland compositors.

%prep
%autosetup

%build
%meson -D%{name}-version=%{version}
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_sysconfdir}/pam.d/%{name}
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/zsh/site-functions/_%{name}
%{_datadir}/fish/completions/%{name}.fish

%changelog
* Sun Aug 04 2019 Jan Geoff Beier <geoff@tuxpup.com> - 1.3-2
- Fix package URL, pull in .sig file

* Thu Mar 14 2019 Jan StanÄ›k <jstanek@redhat.com> - 1.3-1
- Initial package import
