Name:		waybar
Version:	0.7.1
Release:	3%{?dist}
Summary:	Highly customizable Wayland bar for Sway and Wlroots based compositors.

Group:		User Interface/X
License:	MIT
URL:		https://github.com/Alexays/Waybar
Source0:	%{url}/archive/%{version}/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  meson
BuildRequires:  git
BuildRequires:  make
# At least in the COPR build hosts, we need to explicitly specify
# gcc-c++ despite having specified several -devel packages that would seem to pull it in.
BuildRequires:  gcc-c++
BuildRequires:  clang-tools-extra
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(jsoncpp)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(gtkmm-3.0)
BuildRequires:  pkgconfig(libinput)
BuildRequires:  libsigc++20-devel
BuildRequires:  fmt-devel
BuildRequires:  wayland-devel
BuildRequires:  wlroots-devel
BuildRequires:  libnl3-devel
BuildRequires:  libdbusmenu-gtk3-devel
BuildRequires:  libmpdclient-devel
BuildRequires:  spdlog-devel

Requires:       wlroots
Requires:       libnl3
Requires:       libdbusmenu-gtk3
Requires:       libmpdclient

Recommends:     sway

%description
Highly customizable Wayland bar for Sway and Wlroots based compositors.

%prep
%autosetup -p 1 -n Waybar-%{version}

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_bindir}/waybar
%{_sysconfdir}/xdg/waybar/*

%changelog
* Sun Aug 04 2019 Geoff Beier <geoff@tuxpup.com> 0.7.1-1
- Upstream release 0.7.1

* Fri Mar 08 2019 Marvin Beckers <mail@embik.me> 0.4.0-1
- Initial package release

