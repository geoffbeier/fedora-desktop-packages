Name:       mako
Version:	1.4
Release:	3%{?dist}
Summary:	A lightweight Wayland notification daemon

Group:		User Interface/X
License:	MIT
URL:		https://github.com/emersion/mako
Source0:	%{url}/releases/download/v%{version}/%{name}-%{version}.tar.gz
Source1:	%{url}/releases/download/v%{version}/%{name}-%{version}.tar.gz.sig

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  make
BuildRequires:  cmake
BuildRequires:  scdoc
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)

Requires:    cairo
Requires:    pango

%description
A lightweight notification daemon for Wayland.

%prep
%autosetup -p 1 -n %{name}-%{version}
mkdir %{_target_Platform}

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_bindir}/mako*
%{_mandir}/man1/mako*.1*
%{_datadir}/dbus-1/services/fr.emersion.mako.service

%changelog
* Sun Aug 04 2019 Geoff Beier <geoff@tuxpup.com> 1.4-3
- Fix BuildRequires for COPR build environment

* Sun Aug 04 2019 Geoff Beier <geoff@tuxpup.com> 1.4-2
- Package dbus service file

* Sun Aug 04 2019 Geoff Beier <geoff@tuxpup.com> 1.4-1
- Update to upstream 1.4 release

* Sun Mar 10 2019 Marvin Beckers <mail@embik.me> 1.2-1
- Initial package release
